# Visualizzatore di Bounding Box

Il progetto contiene un file C++ e il Makefile per compilarlo.
Scopo del progetto è visualizzare le bounding box di una lista di immagini caricando le annotazioni da dei file testuali.
Per ogni immagine esiste un file testuale contente 0+ labels, nel formato

class   x   y   width   height

* class: nome della classe
* x : coordinata x del centro, normalizzata
* y : coordinata y del centro, normalizzata
* width: lunghezza della bounding box normalizzata
* height: altezza della bounding box normalizzata

La normalizzazione viene fatta usando la lunghezza e l'altezza dell'immagine


## Compilazione su Linux

0. Scaricare OpenCV dal [sito ufficiale](http://opencv.org/) e seguire le istruzioni per compilarlo sulla propria macchina. 
Scaricare CMake dal [sito ufficiale](https://cmake.org/)

1. Clonare il progetto in locale con il comando 

```sh
git clone https://filippoaleotti/visualizzatore-bb
```

2. Aprire CMake ed impostare *src dir* e *output dir* facendole puntare alla directory del progetto
3. Cliccare su *Configure* e selezionare *Unix Makefile*, lasciando le impostazioni di default riguardo al compilatore. Click su *finish*
4. Controllare che vengano trovati automaticamente i riferimenti alla cartella di OpenCV (altrimenti indicarli nella finestra). Click su *Generate*
5. Aprire un terminale, spostarsi nella cartella indicata di destinazione (indicata nel campo *where to build the binaries*) e lanciare il comando

```sh
make
```

6. Eseguire l'applicazione con il comando

```sh
./Distance
```
