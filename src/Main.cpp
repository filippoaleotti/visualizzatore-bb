#include "opencv2/opencv.hpp"
#include <string>


using namespace cv;
using namespace std;

int main(int argc, char**argv)
{
	bool visualizeEverything = false;
	float ratio_th = 0.1;
	char FOLDER_PATH[] = "/home/filippo/Scrivania/blender/Render";	
	char NAME[] = "Chewing_Gum_1";
	char PATH [300]; 
	sprintf(PATH,"%s/%s/Frames/",FOLDER_PATH,NAME);
	char PATH_COORD[400];
	sprintf(PATH_COORD,"%s/%s/Bounding boxes/", FOLDER_PATH, NAME);
	for(int qwerty = 0 ; qwerty<100; qwerty ++)
	{
		
		char frame[500];
		if(qwerty<10)
			sprintf(frame, "0%d",qwerty);
		else
			sprintf(frame, "%d", qwerty);
		cout << frame << endl;

		char left_name[500];
		char right_name[500];
		char hole_txt_name[500];
		char box_txt_name[500];

		sprintf(left_name, "%sframe_00%s_L.png", PATH, frame);
		sprintf(right_name, "%sframe_00%s_R.png", PATH, frame);
		sprintf(hole_txt_name, "%sbb_frame_00%s.txt", PATH_COORD, frame);
		sprintf(box_txt_name, "%sbb_shape_00%s.txt", PATH_COORD, frame);
		cout << left_name << endl;
		cout << right_name << endl;
		cout << hole_txt_name << endl;
		cout << box_txt_name << endl;


		Mat left_img = imread(left_name);
		Mat right_img = imread(right_name);

		if (left_img.data == NULL || right_img.data == NULL) {
			std::cout << "Impossibile caricare le immagini" << std::endl;
			system("PAUSE");
			std::exit(1);
		}

		std::cout << "Immagini caricate" << std::endl;

		Mat bounding_box = left_img.clone();

		//disegno le bounding boxes
		ifstream hole_file, box_file;
		hole_file.open(hole_txt_name);
		string line;
		if (hole_file.is_open())
		{
			while (getline(hole_file, line))
			{
				istringstream string_stream(line);
				string type;
				float x, y, width, height;

				string_stream >> type >> x >> y >> width >> height;
				//NB: se usi una camera stereo e vuoi calcolare le annotazioni di destra
				//a partire da quelle di sinistra devi cambiare lo shift impostandolo con il valore
				//di disparita'
				float shift = 0;

				vector<Point2f> obj_corners(4);
				//x e y sono coordinate del centro in %
				x = x * 640;
				y = y * 480;
				width = width * 640 / 2;
				height = height * 480 / 2;

				//tengo conto dello shift
				x = x - shift;

				obj_corners[0] = Point(x - width, y - height);
				obj_corners[1] = Point(x + width, y - height);
				obj_corners[2] = Point(x + width, y + height);
				obj_corners[3] = Point(x - width, y + height);

				string emptySpace("emptySpace");
				string runningLow("runningLow");

				Scalar color(255, 255, 255);

				if (!type.compare(0, emptySpace.size(), emptySpace))
				{
					color = Scalar(255, 0, 0);
				}
				if (!type.compare(0, runningLow.size(), runningLow))
				{
					color = Scalar(0, 0, 255);
				}

				cv::line(bounding_box, obj_corners[0], obj_corners[1], color, 6);
				cv::line(bounding_box, obj_corners[1], obj_corners[2], color, 6);
				cv::line(bounding_box, obj_corners[2], obj_corners[3], color, 6);
				cv::line(bounding_box, obj_corners[3], obj_corners[0], color, 6);
			}

			hole_file.close();
		}
		else	cout<< "ERRORE: file con le annotazioni non trovato"<<endl;

		box_file.open(box_txt_name);
		if (box_file.is_open())
		{
			while (getline(box_file, line))
			{
				istringstream string_stream(line);
				string type;
				float x, y, width, height;

				string_stream >> type >> x >> y >> width >> height;

				vector<Point2f> obj_corners(4);
				//x e y sono coordinate del centro in %
				x = x * 640;
				y = y * 480;
				width = width * 640 / 2;
				height = height * 480 / 2;

				obj_corners[0] = Point(x - width, y - height);
				obj_corners[1] = Point(x + width, y - height);
				obj_corners[2] = Point(x + width, y + height);
				obj_corners[3] = Point(x - width, y + height);

				cv::line(bounding_box, obj_corners[0], obj_corners[1], Scalar(0, 255, 0), 6);
				cv::line(bounding_box, obj_corners[1], obj_corners[2], Scalar(0, 255, 0), 6);
				cv::line(bounding_box, obj_corners[2], obj_corners[3], Scalar(0, 255, 0), 6);
				cv::line(bounding_box, obj_corners[3], obj_corners[0], Scalar(0, 255, 0), 6);
			}

			hole_file.close();
		}

		char window_name[100];
		sprintf(window_name,"Bounding box %s",frame);
		namedWindow(window_name, WINDOW_AUTOSIZE);
		moveWindow(window_name,10,10);
		imshow(window_name, bounding_box);
		waitKey(0);
		destroyAllWindows();
	}
	cout << "Terminazione programma.."<<endl;
	return 0;
}